package tools;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.codec.binary.Base64;
import play.Logger;
import play.db.DB;
import play.libs.Json;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

/**
 * Created by ghambyte on 2/10/16.
 */
public class Utils {

    public static boolean checkData(String... data) {
        for (String s : data) {
            if (s == null) {
                return false;
            } else if (s.isEmpty()) {
                return false;
            }
        }
        return true;

    }

    public static ObjectNode getObjectNode(int code, String message) {
        ObjectNode result = Json.newObject();

        result.put("code", code);
        result.put("message", message);

        return result;
    }

    public static String getMD5(String chaine) {
        //MessageDigest mDigest;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(chaine.getBytes());

            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++)
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));

            System.out.println("Digest(in hex format):: " + sb.toString());
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static int getTotalRows(String requete) {
        Connection connection = DB.getConnection();
        Statement statement;
        int i;

        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(requete);
            Logger.debug("req " + requete.toString());
            if (resultSet.next()) {
                i = resultSet.getInt("total");
            } else {
                i = 0;
            }
        } catch (SQLException e) {
            Logger.debug("req " + e.getMessage());
            return 0;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                Logger.error("SQLException " + e.getMessage());
            }
        }
        Logger.debug("Total: " + i);
        return i;

    }

    public static int getTotalRowsRestant(String requette) {
        Connection connection = DB.getConnection();
        Statement statement;
        int i;

        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(requette);
            Logger.debug("req " + requette.toString());
            if (resultSet.next()) {
                i = resultSet.getInt("totalrestant");
            } else {
                i = 0;
            }
        } catch (SQLException e) {
            Logger.debug("req " + e.getMessage());
            return 0;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                Logger.error("SQLException " + e.getMessage());
            }
        }
        Logger.debug("Total: " + i);
        return i;

    }

    public static String generateUserPassword() {
        return generateNumber(4);
    }

    public static String generateNumber(int numchars) {
        Random r = new Random();

        StringBuilder builder = new StringBuilder();
        String alphabet = "1234567890";
        for (int i = 0; i < numchars; i++) {
            builder.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        return builder.toString();
    }

    public static ObjectNode getObjectNode(String res, String code, String message) {
        ObjectNode result = Json.newObject();
        result.put("result", res);
        result.put("code", code);
        result.put("message", message);

        return result;
    }

    public static byte[] decodeImage(String picture) {
        return Base64.decodeBase64(picture);
    }

    public static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64URLSafeString(imageByteArray);
    }

}
