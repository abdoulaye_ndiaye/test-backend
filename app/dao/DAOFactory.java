package dao;

/**
 * Created by ghambyte on 9/3/16.
 */
public class DAOFactory {

    private static PostDAO postDAO = null;

    private static UtilisateurDAO utilisateurDAO = null;

    public static PostDAO getPostDAO() {
        if (postDAO != null) {
            return postDAO;
        }

        postDAO = new PostDAOImpl();
        return postDAO;
    }

    public static UtilisateurDAO getUtilisateurDAO() {
        if (utilisateurDAO != null) {
            return utilisateurDAO;
        }

        utilisateurDAO = new UtilisateurDaoImpl();
        return utilisateurDAO;
    }

}
