package dao;

import models.PostModel;
import play.Logger;
import play.db.DB;
import tools.Db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ghambyte on 9/3/16.
 */
public class PostDAOImpl implements PostDAO {

    @Override
    public ArrayList<PostModel> getListPosts(String authorId, String category, String sexe, int page, int per_page){
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
//                "SELECT * FROM posts as po, photos as ph WHERE po.postid =ph.postid AND po.etat='0'");
                "SELECT * FROM posts as po WHERE po.etat='1'");
        if (category != null && !category.isEmpty()){
            req.append("AND category='"+category+"'");
        }
        if (authorId != null && !authorId.isEmpty()){
            req.append("AND authorid='"+authorId+"'");
        }
        req.append(" ORDER BY id DESC");
        if (page == 1) {
            req.append(" LIMIT ").append(per_page);
        } else if (page > -1) {
            req.append(" LIMIT ").append(per_page).append(" OFFSET ")
                    .append((page - 1) * per_page);
        } else {
            req.append(" LIMIT ").append(per_page);
        }
        ArrayList<PostModel> models = new ArrayList<>();
        Logger.debug("getPostsList " + req.toString());
        ResultSet res;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            models = extractModel(res);
            return models;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return models;
        } finally {
            Db.closeQuietly(c);
        }
    }

    @Override
    public ArrayList<PostModel> getPost(String id) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "SELECT * FROM posts WHERE id ='"+ id +"'");

        ArrayList<PostModel> models = new ArrayList<>();
        Logger.debug("getPost " + req.toString());
        ResultSet res;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            models = extractModel(res);
            return models;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return models;
        } finally {
            Db.closeQuietly(c);
        }
    }

    @Override
    public boolean doPost(PostModel postModel) {
        Connection c = DB.getConnection();
        Statement statement;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder req = new StringBuilder(
                "INSERT INTO posts(url, author, category, sexe, authorid, more, datepost, photouser, pays, telephone, etat)" +
                        "VALUES('"+postModel.getAuthor()+"','"+postModel.getUrl()+"','"+postModel.getCategory()+"','"+postModel.getSexe()+
                        "','"+postModel.getAuthorid()+"','"+postModel.getMore()+"','"+dateFormat.format(date)+"','"+postModel.getPhotouser()+"','"+
                        postModel.getPays()+"','"+postModel.getTelephone()+"','"+"0')");

        Logger.debug("Adding Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    @Override
    public boolean deletePost(String id) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "DELETE FROM POSTS" +
                        " WHERE id='"+id+"'");

        Logger.debug("Deleting Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Delete Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    @Override
    public boolean updatePost(String id, PostModel postModel) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "UPDATE posts SET url = "+postModel.getUrl()+", author =" +postModel.getAuthor()+
                        " WHERE id="+id+"");

        Logger.debug("Updating Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Update Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    @Override
    public ArrayList<PostModel> getAllPosts(int page, int per_page, boolean all) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "SELECT * FROM posts ");
        req.append(" ORDER BY id DESC");
        if (!all){
            if (page == 1) {
                req.append(" LIMIT ").append(per_page);
            } else if (page > -1) {
                req.append(" LIMIT ").append(per_page).append(" OFFSET ")
                        .append((page - 1) * per_page);
            } else {
                req.append(" LIMIT ").append(per_page);
            }
        }
        ArrayList<PostModel> models = new ArrayList<>();
        Logger.debug("getAllPosts " + req.toString());
        ResultSet res;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            models = extractModel(res);
            return models;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return models;
        } finally {
            Db.closeQuietly(c);
        }
    }

    @Override
    public boolean validatePost(String id) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "UPDATE posts SET etat ='1'"+
                        " WHERE id="+id+"");

        Logger.debug("Updating Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Update Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    private static ArrayList<String> getPostPhotos(String postid) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder("SELECT * FROM photos where postid='"
                + postid + "'");

        ArrayList<String> listPhoto = null;
        ResultSet res = null;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            listPhoto = extractPhoto(res);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        } finally {
            Db.closeQuietly(c);
        }
        return listPhoto;
    }

    private static ArrayList<String> extractPhoto(ResultSet resultSet)
            throws SQLException {
        ArrayList<String> photos = new ArrayList<>();
        while (resultSet.next()) {
            String p = resultSet.getString("url");
            if (p != null) {
                photos.add(p);
            }
        }
        return photos;
    }

    private static ArrayList<PostModel> extractModel(ResultSet resultSet)
            throws SQLException {
        ArrayList<PostModel> opperationList = new ArrayList<>();
        while (resultSet.next()) {
            PostModel jrCptObject = getPostObject(resultSet);
            if (jrCptObject != null) {
                opperationList.add(jrCptObject);
            }
        }
        return opperationList;
    }

    private static PostModel getPostObject(ResultSet rs) {
        PostModel opp = new PostModel();

        try {
            opp.setId(rs.getString(ID) + "");
            opp.setAuthor(rs.getString(AUTHOR));
            opp.setUrl(rs.getString(URL));
            opp.setAuthorid(rs.getString(AUTHORID));
            opp.setMore(rs.getString(MORE));
            opp.setPhotouser(rs.getString(PHOTOUSER));
            opp.setDate(rs.getString(DATEPOST));
            opp.setCategory(rs.getString(CATEGORY));
            opp.setSexe(rs.getString(SEXE));
            opp.setTelephone(rs.getString(TELEPHONE));
            opp.setPays(rs.getString(PAYS));
            opp.setEtat(rs.getString(ETAT));
            opp.setPosttitre(rs.getString(POSTITRE));
            opp.setPosttexte(rs.getString(POSTTEXTE));
            opp.setUrls(getPostPhotos(rs.getString(POSTID)));
            return opp;
        } catch (SQLException e) {
            Logger.error("Error getPostbject " + e.getMessage());

            return null;
        }
    }
}